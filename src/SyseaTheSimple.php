<?php declare(strict_types=1);

namespace SyseaTheSimple;

use Shopware\Core\Framework\Plugin;
use Shopware\Storefront\Framework\ThemeInterface;

class SyseaTheSimple extends Plugin implements ThemeInterface
{
}