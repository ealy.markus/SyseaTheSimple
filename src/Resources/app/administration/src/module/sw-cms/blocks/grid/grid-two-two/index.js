import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name:               'the-simple-grid-two-two',
    category:           'simple-grid',
    label:              'Grid 2 - 2',
    component:          'the-simple-cms-block-grid-two-two',
    previewComponent:   'the-simple-cms-preview-grid-two-two',
    defaultConfig:      {
        marginBottom:   '20px',
        marginTop:      '20px',
        marginLeft:     '20px',
        marginRight:    '20px',
        sizingMode:     'boxed'
    },
    slots: {
        'grid-one':    'text',
        'grid-two':    'text',
        'grid-three':  'text',
        'grid-four':   'text'
    }
});