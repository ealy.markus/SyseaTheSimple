import './component';
import './config';
import './preview';

Shopware.Service('cmsService').registerCmsElement({
    name: 'highlight-product',
    label: 'sw-cms.elements.highlightProduct.label',
    component: 'sw-cms-el-smililar-products',
    configComponent: 'sw-cms-el-config-smililar-products',
    previewComponent: 'sw-cms-el-preview-smililar-products',
    defaultConfig: {
        customFields: {
            source: 'static',
            value: []
        }
    }
});