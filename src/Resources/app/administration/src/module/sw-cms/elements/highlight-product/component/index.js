import template from './sw-cms-el-highlight-product.html.twig';
import './sw-cms-el-highlight-product.scss';

Shopware.Component.register('sw-cms-el-smililar-products', {
    template,

    mixins: [
        'cms-element'
    ],

    computed: {
        customFields() {
            return this.element.config.customFields.value;
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('smililar-products');
        }
    }
});