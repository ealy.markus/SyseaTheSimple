import template from './sw-cms-el-config-highlight-product.html.twig';

const { Criteria } = Shopware.Data;

Shopware.Component.register('sw-cms-el-config-smililar-products', {
    template,

    inject: ['feature', 'repositoryFactory'],

    mixins: [
        'cms-element',
        'notification',
    ],

    data() {
        return {
            customFieldsData: []
        }
    },

    computed: {
        // Using the repository to work with customFields
        customFieldSetRepository() {
            return this.repositoryFactory.create('custom_field_set');
        },

        // sets the criteria used for your custom field set
        customFieldSetCriteria() {
            const criteria = new Criteria();

            // restrict the customFieldSets to be associated with products
            criteria.addFilter(Criteria.equals('relations.entityName', 'product'));

            // sort the customFields based on customFieldPosition
            criteria
                .getAssociation('customFields')
                .addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

            return criteria;
        },

        cmsCustomFields() {
            if (!this.element.config.customFields.value) {
                return [];
            }

            return this.element.config.customFields.value.map(customField => customField);
        }
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            this.initElementConfig('smililar-products');

            this.loadCustomFields();
        },

        loadCustomFields() {
            this.customFieldSetRepository.search(this.customFieldSetCriteria).then((customFieldSets) => {
                customFieldSets.forEach((set, i) => {
                    if(set !== null) {
                        if('customFields' in set) {
                            let customFields = set.customFields;
                            customFields.forEach((customField, index) => {
                                if(customField !== null) {
                                    let labelTranslations = customField.config.label;
                                    let label = labelTranslations[this.$root.$i18n.locale];
                                    this.customFieldsData.push(
                                        {
                                            value: customField.name,
                                            label: label,
                                        }
                                    )
                                }
                            })
                        }
                    }
                });
            });
        },

        onElementUpdate(element) {
            this.$emit('element-update', element);
        },

        onCustomFieldAdd(customField) {
            this.element.config.customFields.value.push(customField.value);
        },

        onCustomFieldRemove(customField) {
            this.element.config.customFields.value = this.element.config.customFields.value.filter(val => val !== customField.value);
        }
    }
});