import './extension/sw-cms/component/sw-cms-sidebar';

import './module/sw-cms/elements/highlight-product';
import './module/sw-cms/blocks/grid/grid-two-one';
import './module/sw-cms/blocks/grid/grid-two-two';
import deDE from './module/sw-cms/snippet/de-DE.json';
import enGB from './module/sw-cms/snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDE);
Shopware.Locale.extend('en-GB', enGB);