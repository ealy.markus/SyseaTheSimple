$(document).ready(function() {
    function initTopbarSlider() {
        if($('.sysea--topbar-mobile').length) {
            let timeout = $('.sysea--topbar-mobile .owl-carousel').attr('data-autoplay-timeout');
            let animation = $('.sysea--topbar-mobile .owl-carousel').attr('data-autoplay-animation');
            let owlConfig = {
                loop: true,
                margin: 0,
                center: true,
                nav: false,
                dots: false,
                autoplay: true,
                autoplayTimeout: timeout,
                responsive: {
                    0: {
                        items: 1
                    }
                }
            };
            if(animation) {
                owlConfig['animateOut'] = animation;
                if(animation == "fadeOut") {
                    owlConfig['animateIn'] = 'fadeIn';
                }
            }
            $('.sysea--topbar-mobile .owl-carousel').owlCarousel(owlConfig);
        }
    }

    if($(window).width() < 576) {
        initTopbarSlider();
    }
});