import FixedHeaderPlugin from "./plugins/fixed-header/fixed-header.plugin";

const PluginManager = window.PluginManager;
PluginManager.register('FixedHeaderPlugin', FixedHeaderPlugin, '.header-main');

if (module.hot) {
    module.hot.accept();
}