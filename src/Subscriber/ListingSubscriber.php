<?php declare(strict_types=1);

namespace SyseaTheSimple\Subscriber;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Storefront\Page\Wishlist\WishListPageProductCriteriaEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ListingSubscriber implements EventSubscriberInterface
{

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            ProductListingCriteriaEvent::class => 'onProductListingCriteria',
            WishListPageProductCriteriaEvent::class => 'onWishlistCriteria'
        ];
    }

    public function onProductListingCriteria(ProductListingCriteriaEvent $criteriaEvent)
    {
        $criteria = $criteriaEvent->getCriteria();
        $criteria->addAssociations(['properties', 'properties.group', 'manufacturer']);
    }

    public function onWishlistCriteria(WishListPageProductCriteriaEvent $criteriaEvent)
    {
        $criteria = $criteriaEvent->getCriteria();
        $criteria->addAssociations(['properties', 'properties.group', 'manufacturer']);
    }
}